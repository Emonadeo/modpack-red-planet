# Remove redundant "Tough As Nails" Items/Blocks
recipes.remove(<toughasnails:fruit_juice>);
recipes.remove(<toughasnails:fruit_juice:1>);
recipes.remove(<toughasnails:fruit_juice:2>);
recipes.remove(<toughasnails:fruit_juice:4>);
recipes.remove(<toughasnails:fruit_juice:3>);
recipes.remove(<toughasnails:fruit_juice:5>);
recipes.remove(<toughasnails:fruit_juice:6>);
recipes.remove(<toughasnails:fruit_juice:7>);
recipes.remove(<toughasnails:fruit_juice:8>);
recipes.remove(<toughasnails:fruit_juice:9>);

recipes.remove(<toughasnails:rain_collector>);
recipes.remove(<toughasnails:temperature_coil>);
recipes.remove(<toughasnails:thermometer>);
